#!/bin/bash
# DEST_PATH will be removed later
DEST_PATH=/tmp/jekyll_site_of_umrpowered
USERNAME="Manual Deployment"
EMAIL="md@noreply.example.org"

JEKYLL_ENV=production bundle exec jekyll build -d $DEST_PATH

pushd $DEST_PATH
git init
git config user.name "$USERNAME"
git config user.email "$EMAIL"
git checkout -b main
git add .
git commit -m "Upload files to Codeberg Pages"
git remote add origin git@codeberg.org:umrpowered/pages.git
git push -u origin main --force

popd
rm -fr $DEST_PATH
